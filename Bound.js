function Bound(x, y, w, h, t, s) {

   this.color = random(50, 70);


   this.w = w;
   this.h = h;

   this.body = Matter.Bodies.rectangle(x + w/2, y + h/2, w, h, {isStatic: s, restitution: 1, friction: 0});
   this.body.label = t;

   this.draw = function() {
      push();
      fill(this.color, this.color, this.color);
      noStroke();
      rectMode(CENTER);
      translate(this.body.position.x, this.body.position.y)
      rotate(this.body.angle);
      rect(0, 0, this.w, this.h);
      pop();
   }
}