
var ball;

var Engine, World, engine;

var panel;

var bounds = [];

var bricks = [];

var COLS;
var LINES;
var COL_W;
var LINE_H;

var left = false, right = false, up = false;

var BALL_R;

function setup() {

   /* Matter.js initialization */
   Engine = Matter.Engine,
   World = Matter.World;
   engine = Engine.create();

   /* Create P5.js Canvas */
   createCanvas(960, 600);

   /* Create the ball */
   BALL_R = 20;
   ball = new Ball(BALL_R, {x: width/2, y: height/2});
   World.add(engine.world, ball.body);
   Matter.Body.setVelocity(ball.body, {x: 0, y : 20});

   /* Register collisions event */
   Matter.Events.on(engine, 'collisionStart', function(e) {
      for (var i = 0; i < e.pairs.length; ++i) {

         var A = e.pairs[i].bodyA;
         var B = e.pairs[i].bodyB;
         var R = null;


         if (A.label == "brick" && B.label == "ball") {
            R = A;
         } else if (A.label == "ball" && B.label == "brick") {
            R = B;
         } else if (A.label == "panel" && B.label == "ball" || A.label == "ball" && B.label == "panel") {
            var x = 0.1 * (ball.body.position.x - panel.body.position.x); 
            //balls[0].forceToApply = {x: x, y: 0};
            Matter.Body.setVelocity(ball.body, {x: x, y: -25})
         } 

         if (R != null && R.isStatic == true) {

            var index = findBodyObject(R);
            var newB = new Bound(R.position.x - bricks[index].w/2, R.position.y - bricks[index].h/2, bricks[index].w, bricks[index].h, "brick", false);
            newB.color = bricks[index].color;
            bricks.splice(index, 1);
            bricks.push(newB);
            World.add(engine.world, newB.body);
            Matter.World.remove(engine.world, R);
            Matter.Body.setAngularVelocity(newB.body, random(-0.1, 0.1));
         } 

      }
   });
   
   /* Player panel */
   panel = new Bound(width/2 - 100, height - 25, 200, 50, "panel", true);   
   World.add(engine.world, panel.body);

   /* Game area borders */
   bounds.push( new Bound(-50, 0, 50, height * 5, "bound", true));
   bounds.push( new Bound(width, 0, 50, height * 5, "bound", true));
   World.add(engine.world, bounds[0].body);
   World.add(engine.world, bounds[1].body);

   /* Bricks set up variables */
   COLS = 15;
   LINES = 10;
   COL_W = width/COLS;
   LINE_H = (height / 3) / LINES;

   /* Create bricks */
   for (var i = 0; i < LINES; ++i) {
      for (var j = 0; j < COLS; ++j) {
         bricks.push(new Bound(j * COL_W, i * LINE_H, COL_W, LINE_H, "brick", true));
         World.add(engine.world, bricks[i * COLS + j].body);
      }
   }
}

function keyPressed() {
   if(keyCode == LEFT_ARROW) {
      left = true;
   }
   if (keyCode == RIGHT_ARROW) {
      right = true;
   }
   if (keyCode == UP_ARROW) {
      up = true;
   }
}
function keyReleased() {
   if(keyCode == LEFT_ARROW) {
      left = false;
   } else if (keyCode == RIGHT_ARROW) {
      right = false;
   }
   if (keyCode == UP_ARROW) {
      up = false;
   }
}

/* Return the index of the brick object containing the body */
function findBodyObject(body) {
   var index = null;
   for (var i = bricks.length - 1; i >= 0; i--) {
      if (bricks[i].body === body) {
         index = i;
      }
   }

   return index;
}

/* P5.js Draw */
function draw() {
   // Background
   background(255);

   // Update physics
   Engine.update(engine, 1/frameRate);

   /* Translate player panel */
   if (right && panel.body.position.x + panel.w/2 < width) {
         Matter.Body.translate(panel.body, {x: 20, y: 0});
   }
   if (left && panel.body.position.x - panel.w/2 > 0) {
      Matter.Body.translate(panel.body, {x: - 20, y: 0});
   }

   /* draw ball */
   ball.draw();

   /* Draw player */
   panel.draw();

   /* Draw bricks */
   for (var i = bricks.length - 1; i >= 0; --i) {
      bricks[i].draw();
   }

   /* Delete and create new ball if it falls */
   if (ball.body.position.y > height + 200) {
      World.remove(engine.world, ball.body);
      ball = new Ball(BALL_R, {x: panel.body.position.x, y: height/2});
      World.add(engine.world, ball.body);
   }

   /* Delete bricks if they fall */
   for (var i = bricks.length - 1; i >= 0; --i) {
      if (bricks[i].body.position.y > height + 200) {
         World.remove(engine.world, bricks[i].body);
         bricks.splice(i, 1);
      }
   }

   if (bricks.length == 0) {
      location.reload();
      exit();
   }
}