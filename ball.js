function Ball(radius, pos) {
   this.radius = radius;
   this.diameter = 2 * radius;

   this.body = Matter.Bodies.circle(pos.x, pos.y, radius, {restitution: 1, frictionAir: 0.01, friction: 0, frictionStatic: 0});
   this.body.label = "ball";

   this.draw = function() {
      push();
      fill(0);
      noStroke();
      translate(this.body.position.x, this.body.position.y);
      rotate(this.body.angle);
      ellipse(0, 0, this.diameter, this.diameter);
      fill(127);
      ellipse(-this.radius/2, -this.radius/2, this.radius/2, this.radius/2);
      pop();
   }
}

Ball.GRAVITY = {x: 0, y: 1};