# Bricks Breaking Game #

# Simple gravity Bricks Breaking HTML5/Javascript Game #

This game is a simplistic remake of Bricks Breaking, in which gravity takes effect. It uses [Matter.js](http://brm.io/matter-js/) as the physic engine and [P5.js](https://p5js.org/) for rendering.